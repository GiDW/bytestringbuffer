export declare class GByteStringBuffer {
    data: string;
    read: number;
    constructor(bytes?: string | ArrayBuffer | ArrayBufferView | GByteStringBuffer | {
        data: string;
        read: number;
    });
    length(): number;
    /**
     * @param bytes Binary encoded string
     */
    putBytes(bytes: string): GByteStringBuffer;
    putInt32(i: number): GByteStringBuffer;
    getInt32(): number;
    bytes(count?: number): string;
    compact(): GByteStringBuffer;
    toHex(): string;
    /**
     * Returns a UTF-16 string (standard JavaScript string)
     */
    toString(): string;
}
export declare function isArrayBuffer(value: any): value is ArrayBuffer;
export declare function isArrayBufferView(value: any): value is ArrayBufferView;
export declare function encodeUTF8(value: string): string;
export declare function decodeUTF8(value: string): string;
