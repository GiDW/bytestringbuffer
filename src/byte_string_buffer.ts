const isArrayBufferSupported = typeof ArrayBuffer !== 'undefined'

export class GByteStringBuffer {
  data = ''
  read = 0

  constructor (
    bytes?: string | ArrayBuffer | ArrayBufferView | GByteStringBuffer | {
      data: string
      read: number
    }
  ) {
    if (typeof bytes === 'string') {
      this.data = bytes
    } else if (isArrayBuffer(bytes) || isArrayBufferView(bytes)) {
      // TODO
    } else if (bytes instanceof GByteStringBuffer ||
      (
        typeof bytes === 'object' &&
        bytes !== null &&
        typeof bytes.data === 'string' &&
        typeof bytes.read === 'number'
      )) {
      this.data = bytes.data
      this.read = bytes.read
    }
  }

  length (): number {
    return this.data.length - this.read
  }

  /**
   * @param bytes Binary encoded string
   */
  putBytes (bytes: string): GByteStringBuffer {
    this.data += bytes
    return this
  }

  putInt32 (i: number): GByteStringBuffer {
    return this.putBytes(
      String.fromCharCode(i >> 24 & 0xFF) +
      String.fromCharCode(i >> 16 & 0xFF) +
      String.fromCharCode(i >> 8 & 0xFF) +
      String.fromCharCode(i & 0xFF)
    )
  }

  getInt32 (): number {
    return (
      this.data.charCodeAt(this.read++) << 24 ^
      this.data.charCodeAt(this.read++) << 16 ^
      this.data.charCodeAt(this.read++) << 8 ^
      this.data.charCodeAt(this.read++)
    )
  }

  bytes (count?: number): string {
    return typeof count === 'number'
      ? this.data.slice(this.read, this.read + count)
      : this.data.slice(this.read)
  }

  compact (): GByteStringBuffer {
    if (this.read > 0) {
      this.data = this.data.slice(this.read)
      this.read = 0
    }
    return this
  }

  toHex (): string {
    let result = ''
    const length = this.data.length
    for (let i = this.read; i < length; i++) {
      const code = this.data.charCodeAt(i)
      if (code < 16) result += '0'
      result += code.toString(16)
    }
    return result
  }

  /**
   * Returns a UTF-16 string (standard JavaScript string)
   */
  toString (): string {
    return decodeUTF8(this.bytes())
  }
}

export function isArrayBuffer (value: any): value is ArrayBuffer {
  return isArrayBufferSupported && value instanceof ArrayBuffer
}

export function isArrayBufferView (value: any): value is ArrayBufferView {
  return (
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    value &&
    isArrayBuffer(value.buffer) &&
    typeof value.bufferLength !== 'undefined'
  )
}

export function encodeUTF8 (value: string): string {
  return unescape(encodeURIComponent(value))
}

export function decodeUTF8 (value: string): string {
  return decodeURIComponent(escape(value))
}
