module.exports = {
  extends: [
    '@gidw/eslint-config-standard-typescript'
  ],
  parserOptions: {
      project: './tsconfig.json'
  },
  ignorePatterns: [
    'node_modules/',
    'dist/**/*',
    'es/**/*',
    'types/**/*',
    '.eslintrc.js',
    '.eslintrc.cjs'
  ]
}
