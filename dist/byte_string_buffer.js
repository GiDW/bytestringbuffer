"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.decodeUTF8 = exports.encodeUTF8 = exports.isArrayBufferView = exports.isArrayBuffer = exports.GByteStringBuffer = void 0;
var isArrayBufferSupported = typeof ArrayBuffer !== 'undefined';
var GByteStringBuffer = /** @class */ (function () {
    function GByteStringBuffer(bytes) {
        this.data = '';
        this.read = 0;
        if (typeof bytes === 'string') {
            this.data = bytes;
        }
        else if (isArrayBuffer(bytes) || isArrayBufferView(bytes)) {
            // TODO
        }
        else if (bytes instanceof GByteStringBuffer ||
            (typeof bytes === 'object' &&
                bytes !== null &&
                typeof bytes.data === 'string' &&
                typeof bytes.read === 'number')) {
            this.data = bytes.data;
            this.read = bytes.read;
        }
    }
    GByteStringBuffer.prototype.length = function () {
        return this.data.length - this.read;
    };
    /**
     * @param bytes Binary encoded string
     */
    GByteStringBuffer.prototype.putBytes = function (bytes) {
        this.data += bytes;
        return this;
    };
    GByteStringBuffer.prototype.putInt32 = function (i) {
        return this.putBytes(String.fromCharCode(i >> 24 & 0xFF) +
            String.fromCharCode(i >> 16 & 0xFF) +
            String.fromCharCode(i >> 8 & 0xFF) +
            String.fromCharCode(i & 0xFF));
    };
    GByteStringBuffer.prototype.getInt32 = function () {
        return (this.data.charCodeAt(this.read++) << 24 ^
            this.data.charCodeAt(this.read++) << 16 ^
            this.data.charCodeAt(this.read++) << 8 ^
            this.data.charCodeAt(this.read++));
    };
    GByteStringBuffer.prototype.bytes = function (count) {
        return typeof count === 'number'
            ? this.data.slice(this.read, this.read + count)
            : this.data.slice(this.read);
    };
    GByteStringBuffer.prototype.compact = function () {
        if (this.read > 0) {
            this.data = this.data.slice(this.read);
            this.read = 0;
        }
        return this;
    };
    GByteStringBuffer.prototype.toHex = function () {
        var result = '';
        var length = this.data.length;
        for (var i = this.read; i < length; i++) {
            var code = this.data.charCodeAt(i);
            if (code < 16)
                result += '0';
            result += code.toString(16);
        }
        return result;
    };
    /**
     * Returns a UTF-16 string (standard JavaScript string)
     */
    GByteStringBuffer.prototype.toString = function () {
        return decodeUTF8(this.bytes());
    };
    return GByteStringBuffer;
}());
exports.GByteStringBuffer = GByteStringBuffer;
function isArrayBuffer(value) {
    return isArrayBufferSupported && value instanceof ArrayBuffer;
}
exports.isArrayBuffer = isArrayBuffer;
function isArrayBufferView(value) {
    return (
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    value &&
        isArrayBuffer(value.buffer) &&
        typeof value.bufferLength !== 'undefined');
}
exports.isArrayBufferView = isArrayBufferView;
function encodeUTF8(value) {
    return unescape(encodeURIComponent(value));
}
exports.encodeUTF8 = encodeUTF8;
function decodeUTF8(value) {
    return decodeURIComponent(escape(value));
}
exports.decodeUTF8 = decodeUTF8;
