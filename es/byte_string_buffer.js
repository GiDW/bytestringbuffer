const isArrayBufferSupported = typeof ArrayBuffer !== 'undefined';
export class GByteStringBuffer {
    constructor(bytes) {
        this.data = '';
        this.read = 0;
        if (typeof bytes === 'string') {
            this.data = bytes;
        }
        else if (isArrayBuffer(bytes) || isArrayBufferView(bytes)) {
            // TODO
        }
        else if (bytes instanceof GByteStringBuffer ||
            (typeof bytes === 'object' &&
                bytes !== null &&
                typeof bytes.data === 'string' &&
                typeof bytes.read === 'number')) {
            this.data = bytes.data;
            this.read = bytes.read;
        }
    }
    length() {
        return this.data.length - this.read;
    }
    /**
     * @param bytes Binary encoded string
     */
    putBytes(bytes) {
        this.data += bytes;
        return this;
    }
    putInt32(i) {
        return this.putBytes(String.fromCharCode(i >> 24 & 0xFF) +
            String.fromCharCode(i >> 16 & 0xFF) +
            String.fromCharCode(i >> 8 & 0xFF) +
            String.fromCharCode(i & 0xFF));
    }
    getInt32() {
        return (this.data.charCodeAt(this.read++) << 24 ^
            this.data.charCodeAt(this.read++) << 16 ^
            this.data.charCodeAt(this.read++) << 8 ^
            this.data.charCodeAt(this.read++));
    }
    bytes(count) {
        return typeof count === 'number'
            ? this.data.slice(this.read, this.read + count)
            : this.data.slice(this.read);
    }
    compact() {
        if (this.read > 0) {
            this.data = this.data.slice(this.read);
            this.read = 0;
        }
        return this;
    }
    toHex() {
        let result = '';
        const length = this.data.length;
        for (let i = this.read; i < length; i++) {
            const code = this.data.charCodeAt(i);
            if (code < 16)
                result += '0';
            result += code.toString(16);
        }
        return result;
    }
    /**
     * Returns a UTF-16 string (standard JavaScript string)
     */
    toString() {
        return decodeUTF8(this.bytes());
    }
}
export function isArrayBuffer(value) {
    return isArrayBufferSupported && value instanceof ArrayBuffer;
}
export function isArrayBufferView(value) {
    return (
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    value &&
        isArrayBuffer(value.buffer) &&
        typeof value.bufferLength !== 'undefined');
}
export function encodeUTF8(value) {
    return unescape(encodeURIComponent(value));
}
export function decodeUTF8(value) {
    return decodeURIComponent(escape(value));
}
